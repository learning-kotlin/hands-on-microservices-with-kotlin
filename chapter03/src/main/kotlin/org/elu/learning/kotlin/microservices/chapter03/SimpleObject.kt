package org.elu.learning.kotlin.microservices.chapter03

class SimpleObject {
    val name = "hello"
    private val place = "world"
}

class OtherSimpleObject {
    val name = "terve"
    private val zone = "world"
    fun getPlace() = zone
}

data class AnotherSimpleObject(var name: String = "hello", var place: String = "world")

data class ComplexObject(var object1: AnotherSimpleObject? = null)
