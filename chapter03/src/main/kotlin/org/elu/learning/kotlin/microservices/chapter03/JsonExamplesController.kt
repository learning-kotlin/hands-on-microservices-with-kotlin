package org.elu.learning.kotlin.microservices.chapter03

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class JsonExamplesController {
    @GetMapping(value = ["/json"])
    fun getJson() = SimpleObject()

    @GetMapping(value = ["/otherJson"])
    fun getOtherJson() = OtherSimpleObject()

    @GetMapping(value = ["/anotherJson"])
    fun getAnotherJson() = AnotherSimpleObject("hi", "kotlin")

    @GetMapping(value = ["/complexJson"])
    fun getComplexJson() = ComplexObject(object1 = AnotherSimpleObject("more", "complex"))
}
