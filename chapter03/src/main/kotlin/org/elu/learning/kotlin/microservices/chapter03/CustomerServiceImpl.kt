package org.elu.learning.kotlin.microservices.chapter03

import org.elu.learning.kotlin.microservices.chapter03.Customer.Telephone
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class CustomerServiceImpl : CustomerService {
    companion object {
        val initialCustomers = arrayOf(Customer(1, "Kotlin", Telephone("+41", "791234567")),
                Customer(2, "Spring", Telephone("+358", "411234567")),
                Customer(3, "Microservice", Telephone("+34", "201234567")),
                Customer(4, "Boot"))
    }

    val customers = ConcurrentHashMap<Int, Customer>(initialCustomers.associateBy(Customer::id))

    override fun getCustomer(id: Int) = customers[id]

    override fun createCustomer(customer: Customer) {
        customers[customer.id] = customer
    }

    override fun deleteCustomer(id: Int) {
        customers.remove(id)
    }

    override fun updateCustomer(id: Int, customer: Customer) {
        deleteCustomer(id)
        createCustomer(customer)
    }

    override fun searchCustomers(nameFilter: String): List<Customer> =
            customers
                    .filter { it.value.name.contains(nameFilter) }
                    .map { it.value }
}