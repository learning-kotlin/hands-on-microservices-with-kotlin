package org.elu.learning.kotlin.microservices.chapter03

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Chapter03Application

fun main(args: Array<String>) {
    runApplication<Chapter03Application>(*args)
}
