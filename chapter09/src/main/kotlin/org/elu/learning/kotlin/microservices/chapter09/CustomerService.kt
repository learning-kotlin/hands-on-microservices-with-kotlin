package org.elu.learning.kotlin.microservices.chapter09

import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

interface CustomerService {
    fun getCustomer(id: Int): Customer?
    fun getAllCustomers(): List<Customer>
}

@Service
class CustomerServiceImpl : CustomerService {
    companion object {
        private val initialCustomers = arrayOf(Customer(1, "Kotlin"),
                Customer(2, "Spring"),
                Customer(3, "Microservice")
        )
        private val customers = ConcurrentHashMap<Int, Customer>(initialCustomers.associateBy(Customer::id))
    }

    override fun getCustomer(id: Int) = customers[id]

    override fun getAllCustomers() = customers.map(Map.Entry<Int, Customer>::value)
}
