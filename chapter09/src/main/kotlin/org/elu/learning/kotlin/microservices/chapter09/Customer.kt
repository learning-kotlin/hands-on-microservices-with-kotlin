package org.elu.learning.kotlin.microservices.chapter09

data class Customer(val id: Int, val name: String)
