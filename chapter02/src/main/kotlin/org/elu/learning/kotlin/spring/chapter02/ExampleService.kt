package org.elu.learning.kotlin.spring.chapter02

import org.springframework.beans.factory.annotation.Value

interface ServiceInterface {
    fun getHello(name: String): String
}

class ExampleService : ServiceInterface {
    @Value(value = "\${service.message.text:Hello}")
    private lateinit var text: String

    override fun getHello(name: String) = "$text $name"
}

class AdvancedService : ServiceInterface {
    @Value(value = "\${service.message.text:Hello}")
    private lateinit var text: String
    private var count = 0

    override fun getHello(name: String): String {
        count++
        return "$text $name ($count)"
    }
}
