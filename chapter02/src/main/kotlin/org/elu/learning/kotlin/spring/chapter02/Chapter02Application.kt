package org.elu.learning.kotlin.spring.chapter02

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

@SpringBootApplication
class Chapter02Application {
    @Bean
    @ConditionalOnExpression("#{'\${service.message.type}'=='simple'}")
    fun exampleService(): ServiceInterface = ExampleService()

    @Bean
    @ConditionalOnExpression("#{'\${service.message.type}'=='advance'}")
    fun advancedService(): ServiceInterface = AdvancedService()
}

@Controller
class FirstController {
    @Autowired
    lateinit var service: ServiceInterface

    @RequestMapping(value = ["/user/{name}"], method = [(RequestMethod.GET)])
    @ResponseBody
    fun hello(@PathVariable name: String) = service.getHello(name)
}

fun main(args: Array<String>) {
    runApplication<Chapter02Application>(*args)
}
