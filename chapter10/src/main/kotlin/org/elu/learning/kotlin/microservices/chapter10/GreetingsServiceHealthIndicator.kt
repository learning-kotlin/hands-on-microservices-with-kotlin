package org.elu.learning.kotlin.microservices.chapter10

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.stereotype.Component

@Component
class GreetingsServiceHealthIndicator : HealthIndicator {
    @Autowired
    lateinit var greetingsService: GreetingsService

    override fun health(): Health {
        return try {
            val message = greetingsService.getGreeting()
            Health.up().withDetail("Message", message).build()
        } catch (exception: Exception) {
            Health.down().withDetail("ERROR", exception.message).build()
        }
    }
}
