package org.elu.learning.kotlin.microservices.chapter10

import org.springframework.stereotype.Service
import java.util.*

interface GreetingsService {
    fun getGreeting(): String
}

@Service
class GreetingsServiceImpl : GreetingsService {
    companion object {
        private val greetingsMessages = arrayOf("Hello", "Olá", "Namaste", "Hola")
    }

    fun random(max: Int): Int = Random().nextInt(max) + 1

    override fun getGreeting() = greetingsMessages[random(4)]
}
