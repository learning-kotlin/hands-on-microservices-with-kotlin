package org.elu.learning.kotlin.microservices.chapter06microservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Chapter06MicroserviceApplication

fun main(args: Array<String>) {
    runApplication<Chapter06MicroserviceApplication>(*args)
}
